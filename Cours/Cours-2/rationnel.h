/* rationnel.h */

/* IMPLANTATION DU TYPE 
   SPECIFICATIONS :
   data pointe vers un tableau de 2 int alloués dans le tas.
   data[0] = numer
   data[1] = denom

   numer contient le numérateur
   denom contient le dénominateur
   La fraction est réduite : numer et denom sont premiers entre eux
   Le signe est porté par le numérateur (denom est tjs positif)
 */

struct rationnel {
    int* data;
};

/* PROTOTYPES DES FONCTIONS EXPORTEES */

/* Constructeur. 
   Initialise A avec la fraction n/d.
   Le dénominateur d est non nul.
 */

extern void init_rationnel (struct rationnel* A, int n, int d);

extern void clear_rationnel (struct rationnel*);

/* Affecte à S (mode R) le rationnel A + B (A, B mode D) */
extern void add_rationnel 
            (struct rationnel* S, struct rationnel* A, struct rationnel* B);

extern void print_rationnel (struct rationnel*);

