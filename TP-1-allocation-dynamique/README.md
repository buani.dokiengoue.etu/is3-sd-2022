Trois CM, deux TD et deux TP de 2h

CM 1
    Pour fabriquer un nouveau type en C (une structure) :
    - on crée un fichier entête : déclarations de type, 
        prototypes des actions/fonctions exportées, spécifications
    - on crée un fichier source : l'implantation des actions/fonctions
    Compilation séparée, constructeur et destructeur

CM 2
    Allocation dynamique (malloc, free), valgrind, gdb
    Le mécanisme de gestion des variables locales (pile d'excécution)
    Le mécanisme des constructeurs et destructeurs permet d'organiser
        le code de façon rationnelle. C'est un guide très utile en cas
        d'allocation dynamique.

CM 3
    Les listes chaînées

TD 1
    Gestion de la mémoire élémentaire

TD 2
    Listes chaînées

    Note : les étudiants et apprentis sont incités à implanter leur 
        propre module de listes chaînées (CM3 et TD2) mais celles du poly
        sont disponibles au cas où

TP 1
    Implantations d'un module de chaînes de caractères
    Version 1 : un malloc et un free dans un fichier .c
    Version 2 : plusieurs realloc et un free dans un fichier .c

TP 2
    Version 3 : avec création d'un module séparé (chaine.h et chaine.c)
    Version 4 : modification de l'implantation en adaptant le module
        de listes chaînées (de doubles) en un module de listes de 
        caractères
    Version 5 (optionnelle) : remplacement des char par des wchar_t

Technologies : 
    l'utilitaire valgrind (très, très conseillé)
    le debugger gdb (très conseillé)
    gcc -c -g 
    l'utilitaire make (conseillé mais rarement fait)

