/*liste.h*/
/*Implémentation des liste chaînées*/
/*Ces listes sont constituées de maillons*/
/*Les liste sont situées dans la pile */
/* le maillon tete pointe vers le premier maillon de la liste*/
/*Lorsque la liste est vide la tete vaut NIL*/


struct liste { 
    struct maillon* tete;
    struct maillon* queue;
    double nb;
};


/* un maillon est alloué dynamiquement dans le tas*/
/* il possède une valeur et un champ suivant qui pointevers le prochain maillon dans la liste*/


struct maillon {
    
    double valeur;
    struct maillon* suivant;
};




#define NIL (struct maillon*)0

extern void init_liste (struct liste*);
extern void clear_liste(struct liste*);
extern void ajout_en_tete (struct liste*, double );
extern void ajout_en_queue(struct liste*, double );
extern void impression (struct liste L);





