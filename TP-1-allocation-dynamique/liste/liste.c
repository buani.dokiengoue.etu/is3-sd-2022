#include<stdio.h>
#include<stdlib.h>
#include <stdbool.h>
#include"liste.h"



void init_liste (struct liste* L){
    
    L->tete=NIL;
    L->queue=NIL;
    L->nb=0;
    
}

void ajout_en_tete (struct liste* L, double d){
    
    struct maillon*M;
    M=(struct maillon*) malloc(sizeof(struct maillon));
      M->valeur=d;
    M->suivant=L->tete;
    L->tete=M;
   
    L->nb=L->nb+1;
    
    
}

void ajout_en_queue (struct liste* L, double d){
    
    /* allocation mémoire du maillon que l'on veut insérer*/
    struct maillon*M;
    M=(struct maillon*) malloc(sizeof(struct maillon));
    M->valeur=d;
    M->suivant=NIL;
    
    /*cas d'une liste vide*/
    if(L->tete==NIL){
        L->tete=M;
        L->queue=M;       
    }
        
        else {
            L->queue->suivant=M;
            L->queue=M;
        }
        
        L->nb+=1;
}
void clear_liste  (struct liste* L){
    
    int i;
    struct maillon* courant;
    struct maillon* suivant;
    courant=L->tete;
    
    for(i=0;i<L->nb;i++){
        
        suivant=courant->suivant;
        free(courant);
        courant=suivant;
}


}


void impression(struct liste L){
   
    int i;
    bool p;
    struct maillon* M;
    
    M=L.tete;
    p=true;
    
    for(i=0;i<L.nb;i++){
        
        if(p)
            printf("%lf ",M->valeur);
          else
          {
              printf(";%lf ",M->valeur);}
          
              p=false;
              M=M->suivant;
}

}

