#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define SIZE_MAX 20
struct ensemble
{
    int size;
    int elt[SIZE_MAX];
};

void
init_ensemble (struct ensemble *E)
{
    E->size = 0;
}

void
print_ensemble (struct ensemble *E)
{
    int i;
    putchar ('{');
    if (E->size > 0)
      {
          printf ("%d", E->elt[0]);
          for (i = 1; i < E->size; i++)
              printf (", %d", E->elt[i]);
      }
    puts ("}\n");
}

bool
appartient (int b, struct ensemble *E)
{
    bool found;
    int i;
    found = false;
    for (i = 0; i < E->size && !found; i++)
        found = E->elt[i] == b;
    return found;
}

void
erreur (void)
{
    fprintf (stderr, "une erreur s'est produite\n");
    exit (1);
}

void
tirage_loto (void)
{
    struct ensemble boules, tirage;
    int i, n, k;
    init_ensemble (&boules);
    init_ensemble (&tirage);
/* � chaque it�ration, la structure "boules" est coh�rente */
    for (i = 0; i < SIZE_MAX; i++)
      {
          boules.elt[boules.size] = i + 1;
          boules.size += 1;
      }
/* n = le nombre de boules � tirer */
    srand48 ((long) time (NULL));
    n = (int) (drand48 () * (SIZE_MAX + 1));
    for (i = 0; i < n; i++)
      {
          k = (int) (drand48 () * (boules.size + 1));   /* <- bug ! */
          if (appartient (boules.elt[k], &tirage))
              erreur ();
          tirage.elt[tirage.size] = boules.elt[k];
          tirage.size += 1;
/* ici, la structure "tirage" est coh�rente */
          boules.elt[k] = boules.elt[boules.size - 1];
          boules.size -= 1;
/* ici, la structure "boules" est coh�rente */
          if (boules.size < 0 || tirage.size > SIZE_MAX)
              erreur ();
      }
    print_ensemble (&tirage);
}

int
main ()
{
    tirage_loto ();
    return 0;
}
